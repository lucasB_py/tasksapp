import { Component } from '@angular/core';
import { TaskService } from '../services/task.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.page.html',
  styleUrls: ['./tasks.page.scss'],
})
export class TasksPage {
  tasks: any = [];

  constructor( private taskService: TaskService,
    private alertControler: AlertController) {}

  loadTasks() {
    this.taskService.getTasks().subscribe(
      (res) => {
        this.tasks = res;
      },
      (err) => console.error(err)
    );
  }

  async deleteTask(id) {
    const alert = await this.alertControler.create({
      header: 'Eliminar',
      subHeader: 'Eliminar esta tarea',
      message: 'Estas seguro de eliminar esta tarea?',
      buttons: [{
        text: 'Eliminar',
        handler: () => {
          console.log(id);
          this.taskService.deleteTask(id).subscribe(
            (res) => {
              this.loadTasks();
            },
            (err) => console.error(err));
        }
      }, 'Cancelar']
    });

    await alert.present();
  }

  editTask(id) {
    console.log(id)
  }

  ionViewWillEnter() {
    this.loadTasks();
  }
}
