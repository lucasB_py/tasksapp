import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Task {
  id?: string;
  title: string;
  description: string;
}

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  urlAPI = 'http://localhost:1337/tasks';

  constructor(private http: HttpClient) {}

  getTasks() {
    return this.http.get(this.urlAPI);
  }

  getTaskById(id: string) {
    return this.http.get<Task>(`${this.urlAPI}/${id}`);
  }

  createTask(title: string, description: string) {
    return this.http.post(this.urlAPI, {
      title,
      description,
    });
  }

  updateTask(id: string, task: Task) {
    return this.http.put(`${this.urlAPI}/${id}`, task);
  }

  deleteTask(id: string) {
    return this.http.delete(`${this.urlAPI}/${id}`);
  }
}
