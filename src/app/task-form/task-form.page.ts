import { Component, OnInit } from '@angular/core';
import { TaskService, Task } from '../services/task.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.page.html',
  styleUrls: ['./task-form.page.scss'],
})
export class TaskFormPage implements OnInit {
  editing = false;
  task: Task = {
    title: '',
    description: '',
  };

  constructor(
    private apiServices: TaskService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      if (paramMap.get('taskId')) {
        this.editing = true;
        this.apiServices
          .getTaskById(paramMap.get('taskId'))
          .subscribe((res) => {
            this.task = res;
            console.log(this.task);
          });
      }
    });
  }

  saveTask() {
    this.apiServices
      .createTask(this.task.title, this.task.description)
      .subscribe(
        (res) => {
          this.router.navigate(['/tasks']);
        },
        (err) => console.error(err)
      );
  }

  updateTask() {
    this.apiServices
      .updateTask(this.task.id, {
        title: this.task.title,
        description: this.task.description,
      })
      .subscribe((res) => {
        this.router.navigate(['/tasks']);
      });
  }
}
